﻿using UnityEngine;
using System;

namespace InventorySystem
{
    public enum InventoryItemAttributeType
    {
        Strength,
        Intellect,
        Dexterity,
        Wisdom,
        Endurance,
        Luck,
        DPS,
        CritDamageMin,
        CritDamageMax,
        Speed,
        Level
    }

    [Serializable]
    public class InventoryItemAttribute
    {
        public InventoryItemAttributeType attr;
        public float value;
    }

    public enum Rarity
    {
        Awesome,
        Epic,
        Extreme,
        Unmatched,
        Unknown,
        None
    }
}