﻿namespace InventorySystem
{
    public enum InventoryItemType
    {
        Weapons,
        Armour,
        Food,
        Spells,
        QuestItems,
        Generic
    }
}