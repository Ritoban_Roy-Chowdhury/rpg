﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace InventorySystem
{
    public class OpenInventory : MonoBehaviour
    {
        public string inventoryAxis;
        public float speed = 3;
        public float endX = 0f;
        public static bool openNext = true;

        private void Update()
        {
            if (Input.GetAxisRaw(inventoryAxis) == 1 && openNext)
            {
                gameObject.SetActive(true);
                StartCoroutine(MoveFromLeft());
            }
        }

        private IEnumerator MoveFromLeft()
        {
            while (transform.position.x < endX)
            {
                Vector3 pos2 = transform.position;
                pos2.x += speed;
                transform.position = pos2;
                yield return null;
            }
            Vector3 pos = transform.position;
            pos.x = endX;
            transform.position = pos;
            CloseInventory.closeNext = true;
            openNext = false;
            StopAllCoroutines();
        }

    }
}
