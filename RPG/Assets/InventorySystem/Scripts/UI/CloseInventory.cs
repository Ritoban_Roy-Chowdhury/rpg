﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace InventorySystem
{
    public class CloseInventory : MonoBehaviour
    {
        public string inventoryAxis;
        public float speed = 3;
        public float endX = 0f;
        public static bool closeNext = false;

        private void Update()
        {
            if (Input.GetAxisRaw(inventoryAxis) == 1 && closeNext)
            {
                
                StartCoroutine(MoveToRight());
            }
        }

        private IEnumerator MoveToRight()
        {
            while (transform.position.x > endX)
            {
                Vector3 pos2 = transform.position;
                pos2.x -= speed;
                transform.position = pos2;
                yield return null;
            }
            Vector3 pos = transform.position;
            pos.x = endX;
            transform.position = pos;
            OpenInventory.openNext = true;
            closeNext = false;  
            StopAllCoroutines();
        }

    }
}
