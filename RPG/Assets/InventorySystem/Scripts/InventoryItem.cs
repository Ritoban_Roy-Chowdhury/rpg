﻿using UnityEngine;
using UnityEngine.UI;

namespace InventorySystem
{
    [CreateAssetMenu(fileName = "New Inventory Item", menuName = "Inventory/Inventory Item", order = 0)]
    public class InventoryItem : ScriptableObject
    {
        public string m_name;
        public InventoryItemType type;
        public Sprite image;
        public bool canEquip;
        public GameObject equpPrefab;
        public InventoryItemAttribute[] attributes;
        public Rarity rarity;
    }
}
