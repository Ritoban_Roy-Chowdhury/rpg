﻿using UnityEditor;
using UnityEngine;

public class DrawRect : EditorWindow
{
    // Add menu item named "My Window" to the Window menu
    [MenuItem("Window/GUI Tests/Draw Rect Window")]
    public static void ShowWindow()
    {
        //Show existing window instance. If one doesn't exist, make one.
        EditorWindow.GetWindow(typeof(DrawRect));
    }

    public void DrawNode(Vector2 pos, Vector2 size)
    {
        EditorGUI.DrawRect(new Rect(pos, size), new Color(0.5f, 0.5f, 0.5f));
    }

    void OnGUI()
    {
        EditorGUI.DrawRect(new Rect(new Vector2(0, 0), position.size), new Color(0.2f, 0.2f, 0.2f));
        DrawNode(new Vector2(80, 80), new Vector2(150, 100));
    }
}
