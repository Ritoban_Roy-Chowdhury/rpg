﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System;

public class NodeEditor : EditorWindow
{
    public delegate void DrawDelegate(int id);
    List<Node> nodes = new List<Node>();
    List<Rect> nodePos = new List<Rect>();

    InOutInfo currentSelectedInput;
    InOutInfo secondConnect;


    static Color pressedColor = new Color(0.3f, 0.3f, 0.3f);
    static Color normalColor = new Color(0.5f, 0.5f, 0.5f);

    [MenuItem("Window/GUI Tests/Node Editor")]
    public static void ShowWindow()
    {
        //Show existing window instance. If one doesn't exist, make one.
        EditorWindow.GetWindow(typeof(NodeEditor));
    }

    private void OnGUI()
    {
        EditorGUI.DrawRect(new Rect(new Vector2(0, 0), position.size), new Color(0.2f, 0.2f, 0.2f));

        BeginWindows();
        CreateNode("Start Node", new Vector2(20, 20), new Vector2(100, 150), 0, "This is the first node!", DrawStartNode, new InOutInfo("Start Node Input 1", true));


        CreateNode("Second Node", new Vector2(20, 20), new Vector2(100, 150), 1, "This not is the first node!", DrawMiddleNode, new InOutInfo("2- Input 1", false), new InOutInfo("2- Output 2", true));
        EndWindows();

        if(currentSelectedInput != null)
        {
            
        }
        
        if (secondConnect != null && currentSelectedInput != null)
        {
            Debug.Log("Start: " + currentSelectedInput.name);
            Debug.Log("End: " + secondConnect.name);
            secondConnect = null;
            currentSelectedInput = null;
        }
        
    }

    private void CreateNode(string name, Vector2 position, Vector2 size, int id, string text, DrawDelegate draw, params InOutInfo[] inputs)
    {
        Node node = new Node(name, position, size, id, text);
        List<InOutInfo> inputList = new List<InOutInfo>();
        foreach (InOutInfo info in inputs)
        {
            info.color = normalColor;
            info.pressedColor = pressedColor;
            info.parentNode = node.id;
            inputList.Add(info);
        }
        node.inOutList = inputList;
        node.draw += draw;
        nodes.Add(node);
        nodePos.Add(node.nodeRect);
        CreateWindowFromNode(node);
    }

    private void CreateWindowFromNode(Node node)
    {
        nodePos[node.id] = GUILayout.Window(node.id, nodePos[node.id], DrawNode, node.name);
    }

    private void DrawNode(int id)
    {
        EditorGUILayout.LabelField(nodes[id].name, EditorStyles.boldLabel);
        nodes[id].draw(id);
        GUI.DragWindow();
    }

    private void DrawTextField(int id)
    {
        nodes[id].text = EditorGUILayout.TextArea(nodes[id].text);
    }

    private void DrawStartNode(int id)
    {
        DrawTextField(id);
        DrawInOutRects(id);
    }

    private void DrawInOutRects(int id)
    {
        Rect inputRect;
        for (int i = 0; i < nodes[id].inOutList.Count; i++)
        {
            InOutInfo info = nodes[id].inOutList[i];
            Color col;

            if (nodes[id].inOutList[i].output)
                inputRect = new Rect(nodePos[id].size.x - 20, 80 + (i * 25), 20, 20);
            else
                inputRect = new Rect(0, 80 + (i * 25), 20, 20);


            if (inputRect.Contains(Event.current.mousePosition) && Event.current.type == EventType.mouseUp)
            {
                if (currentSelectedInput == null)
                {
                    currentSelectedInput = info;
                    
                }
                else if (currentSelectedInput != info && currentSelectedInput.output != info.output && currentSelectedInput.parentNode != info.parentNode)
                {
                    secondConnect = info;
                }
            }

            if(currentSelectedInput == info)
            {
                col = info.pressedColor;
            }
            else
            {
                col = info.color;
            }
            EditorGUI.DrawRect(inputRect, col);
        }
    }


    private void DrawMiddleNode(int id)
    {
        DrawTextField(id);
        DrawInOutRects(id);

    }
}


public class Node
{
    public static Color nodeColor = new Color(0.5f, 0.5f, 0.5f);
    public string name;
    public Vector2 position;
    public Vector2 size;
    public int id = 0;
    public string text;
    public NodeEditor.DrawDelegate draw;
    public List<InOutInfo> inOutList;
    public Rect nodeRect
    {
        get
        {
            return new Rect(position, size);
        }
    }

    public Node(string name, Vector2 position, Vector2 size, int id, string text)
    {
        this.name = name;
        this.size = size;
        this.position = position;
        this.id = id;
        this.text = text;
    }
}

public class InOutInfo
{
    public Color color;
    public Color pressedColor;
    public int parentNode;
    public bool output;
    public string name;

    public InOutInfo(string name, bool output)
    {
        this.name = name;
        this.output = output;
    }

}