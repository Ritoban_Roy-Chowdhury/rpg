﻿using UnityEngine;
using System.Collections;
using System;

namespace CharacterSystem
{

    public class PlayerController : MonoBehaviour
    {
        public float walkSpeed;
        public float runSpeed;
        public float crouchWalkSpeed;
        public float animSpeedMultiplier = 5;

        private Transform cam;
        private Animator anim;
        private CapsuleCollider capsule;
        private Rigidbody playerRigidbody;

        private float h;
        private float v;
        private bool run;
        private bool crouching;
        private bool jump;

        private float crouchTimer;
        private float jumpTimer;

        private Vector3 capsuleOffset;

        private void Awake()
        {
            playerRigidbody = GetComponent<Rigidbody>();
            anim = GetComponent<Animator>();
            capsule = GetComponent<CapsuleCollider>();
            cam = Camera.main.transform;
            if (cam == null)
                cam = transform;
        }


        private void Update()
        {
            crouchTimer += Time.deltaTime;
            jumpTimer += Time.deltaTime;
            v = Input.GetAxis("Vertical");
            h = Input.GetAxis("Horizontal");
            run = Input.GetKey(KeyCode.LeftShift);
            if (Input.GetKey(KeyCode.C) && crouchTimer > 0.2)
            {
                crouching = !crouching;
                crouchTimer = 0f;
            }
            if(Input.GetKey(KeyCode.Space) && jumpTimer > 0.2)
            {
                jump = true;
                jumpTimer = 0f;
            }
            else
            {
                jump = false;
            }
        }

        private void FixedUpdate()
        {
            Vector3 movement = new Vector3();
            movement += v * cam.forward;
            movement += h * cam.right;
            float speed = (run) ? runSpeed : walkSpeed;
            if (crouching)
                speed = crouchWalkSpeed;
            movement = movement.normalized * speed * Time.fixedDeltaTime;
            movement.y = 0;


            playerRigidbody.MovePosition(playerRigidbody.position + movement);
            if(movement != Vector3.zero)
                playerRigidbody.MoveRotation(Quaternion.Slerp(playerRigidbody.rotation, Quaternion.LookRotation(movement), 10 * Time.fixedDeltaTime));
            if (jump)
                anim.SetTrigger("Jumping");

            anim.SetFloat("Speed", movement.magnitude * animSpeedMultiplier);
            anim.SetBool("Crouching", crouching);

        }
    }
}