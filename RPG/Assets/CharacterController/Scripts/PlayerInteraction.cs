﻿using UnityEngine;
using UnityEngine.UI;


public class PlayerInteraction : MonoBehaviour
{
    public int layer;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == layer)
        {
            print(other.name);
        }
    }
}
